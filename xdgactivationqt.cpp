#include "xdgactivationqt.h"

#include "activation.h"

#include <QGuiApplication>

#include <private/qwaylanddisplay_p.h>
#include <private/qwaylandinputdevice_p.h>
#include <private/qwaylandwindow_p.h>
#include <qpa/qplatformnativeinterface.h>

#include <QDebug>

XdgActivationQt::XdgActivationQt(QObject *parent)
    : QObject(parent)
{
    m_activation = new WaylandXdgActivationV1;
}

XdgActivationQt::~XdgActivationQt()
{
}

quint32 lastInputSerial(QWindow *window)
{
    auto waylandWindow = window ? dynamic_cast<QtWaylandClient::QWaylandWindow *>(window->handle()) : nullptr;
    Q_ASSERT(waylandWindow);
    return waylandWindow->display()->lastInputSerial();
}

wl_surface *surfaceForWindow(QWindow *window)
{
    QPlatformNativeInterface *native = qGuiApp->platformNativeInterface();
    Q_ASSERT(native);
    wl_surface *s = reinterpret_cast<wl_surface *>(native->nativeResourceForWindow(QByteArrayLiteral("surface"), window));
    Q_ASSERT(s);
    return s;
}

wl_seat *seatForWindow(QWindow *window)
{
    auto waylandWindow = window ? dynamic_cast<QtWaylandClient::QWaylandWindow *>(window->handle()) : nullptr;
    Q_ASSERT(waylandWindow);
    return waylandWindow->display()->defaultInputDevice()->wl_seat();
}

void XdgActivationQt::requestTokenFor(QWindow *window, const QString &appName)
{
    auto tokenRequest = m_activation->requestXdgActivationToken(seatForWindow(window), surfaceForWindow(window), lastInputSerial(window), appName);

    connect(tokenRequest, &WaylandXdgActivationTokenV1::done, this, [this](const QString &token) {
        Q_EMIT tokenArrived(token);
    });
}

void XdgActivationQt::activateWindow(QWindow *window, const QString &token)
{
    m_activation->activate(token, surfaceForWindow(window));
}
