#pragma once

#include <QObject>

#include <QtWaylandClient/qwaylandclientextension.h>

#include "qwayland-xdg-activation-v1.h"

class WaylandXdgActivationTokenV1 : public QObject, public QtWayland::xdg_activation_token_v1
{
    Q_OBJECT
public:
    void xdg_activation_token_v1_done(const QString &token) override
    {
        Q_EMIT done(token);
    }

Q_SIGNALS:
    void done(const QString &token);
};

class WaylandXdgActivationV1 : public QWaylandClientExtensionTemplate<WaylandXdgActivationV1>, public QtWayland::xdg_activation_v1
{
    Q_OBJECT
public:
    WaylandXdgActivationV1();

    WaylandXdgActivationTokenV1 *requestXdgActivationToken(wl_seat *seat, struct ::wl_surface *surface, uint32_t serial, const QString &app_id);
};
