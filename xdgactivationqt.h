#pragma once

#include <QObject>

#include <QWindow>

class WaylandXdgActivationV1;

class XdgActivationQt : public QObject
{
    Q_OBJECT
public:
    explicit XdgActivationQt(QObject *parent = nullptr);
    ~XdgActivationQt() override;

    /**
     * Requests a new activation request
     * @param window The window requesting a token. This is the window that currently has focus, *not* the window being activated.
     * @param appName The desktop entry name (e.g. "org.kde.kate") of the application being activated. This is optional.
     * Once the token is ready tokenArrived is emitted.
     */
    void requestTokenFor(QWindow *window, const QString &appName = QString());

    /**
     * Emitted when the requested token arrived.
     * @param token the new token that can be used now.
     */
    Q_SIGNAL void tokenArrived(const QString &token);

    /**
     * Raises the window.
     * @param window The window to be raised.
     * @param token the token to authorize this request
     */
    void activateWindow(QWindow *window, const QString &token);

private:
    WaylandXdgActivationV1 *m_activation;
};
