#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QDebug>
#include <QDesktopServices>
#include <QUrl>

#include "xdgactivationqt.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    activation = new XdgActivationQt(this);

    connect(activation, &XdgActivationQt::tokenArrived, this, &MainWindow::gotToken);

    QMainWindow *w = new QMainWindow(this);
    w->show();
    w->setWindowState(Qt::WindowMinimized);
    otherWindow = w->windowHandle();

    connect(ui->pushButton, &QPushButton::clicked, this, [this] {
        activation->requestTokenFor(windowHandle());
    });
}

void MainWindow::gotToken(const QString &token)
{
    activation->activateWindow(otherWindow, token);
}

MainWindow::~MainWindow()
{
    delete ui;
}
